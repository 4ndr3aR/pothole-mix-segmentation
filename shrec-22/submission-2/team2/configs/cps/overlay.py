#!/usr/bin/env python3
import sys
import numpy as np
from PIL import Image
from pathlib import Path
from fastai.vision.core import load_image
import cv2

def overlay(frame, rgb_mask, alpha=0.5, debug=False):
	if frame.shape != rgb_mask.shape:
		rgb_mask = np.copy(rgb_mask)
		rgb_mask = Image.fromarray(np.uint8(rgb_mask))
		rgb_mask = rgb_mask.resize((frame.shape[1], frame.shape[0]))
		rgb_mask.save('msk.png')
		rgb_mask = np.array(rgb_mask)

	if debug:
		print('a', type(frame), frame.shape)
		print('b', type(rgb_mask), rgb_mask.shape)

	return cv2.addWeighted(frame, 1, rgb_mask, alpha, 0)

def load_img_from_string(input_img, debug=False):
	if isinstance(input_img, str) or isinstance(input_img, Path):
		if debug:
			print(f'Opening image/mask: {input_img}')
		img_fn = input_img
		img    = load_image(input_img)
		if debug:
			print(f'Opened image/mask {input_img} with size: {img.shape}')
	return img_fn, img

def prepare_img(input_img, debug=False):
	img_fn = None
	if isinstance(input_img, str) or isinstance(input_img, Path):
		img_fn, img = load_img_from_string(input_img, debug=debug)
		img = np.array(img)
	elif isinstance(input_img, PIL.Image.Image):
		if debug:
			print(f'Received image/mask {type(img)} with size: {img.shape}')
		img = np.array(img)
	else:
		print(f'Input image/mask in unknown format ({type(input_img)}). Exiting...')
		return
	return img_fn, img


if __name__ == '__main__':
	debug = True

	if debug:
		print(f'Received {len(sys.argv)} parameters: {sys.argv}')

	if len(sys.argv) >= 2:
		input_img  = sys.argv[1]
		if input_img != "":
			img_fn, img = prepare_img(input_img, debug=True)
	else:
		print(f'No image specified. Exiting...')
		sys.exit(1)

	if len(sys.argv) >= 3:
		input_mask = sys.argv[2]
		if input_mask != "":
			mask_fn, mask = prepare_img(input_mask, debug=debug)

		print(type(img), type(mask))
		overlay_img = overlay(img, mask, debug=debug)
		print(type(overlay_img))
		overlay_img = Image.fromarray(overlay_img)

		if img_fn is not None:
			out_fn = Path('/tmp') / ('blend-' + Path(img_fn).name.replace('.png', '.jpg'))
			print(f'Saving image: {out_fn}')
			overlay_img.save(out_fn)
	else:
		print(f'No mask specified. Exiting...')
		sys.exit(2)
