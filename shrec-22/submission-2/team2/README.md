# <p align="center"> HCMUS Team at SHREC'22: Pothole and crack detection on road pavement </p>

<p align="center"> <img height="300" alt="screen" src="./docs/figures/overlays.png"> </p>

----------------------------------------------------------

## Table of contents

- [About the challenge](./docs/SHREC22.md)
- [Our methods](./docs/SUBMISSION.md)
- [Datasets](./docs/DATA.md)
- How to run:
  - [Training and evaluation](./docs/CODE.md)
  - [Inference](./docs/INFERENCE.md)
- [Results](./docs/RESULTS.md)
- [References](./docs/REFERENCES.md)

----------------------------------------------------------

# For the track organizers!!
## Please read [Our 3 runs descriptions](./docs/SUBMISSION.md) and [How to inference](./docs/INFERENCE.md) on folder of images and video.
## We submit the source code and `ckpt` folder contains model's checkpoints; also an additional notebook which provides more detailed guidance.