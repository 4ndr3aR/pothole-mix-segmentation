#!/bin/bash

source /mnt/btrfs-data/venvs/theseus/bin/activate

for i in `cat /mnt/btrfs-data/venvs/ml-tutorials/repos/segmentation_inference/shrec-2022/shrec-2022-video-evaluation-file-list.txt` ; do echo $i ; ls -lh $i ; python configs/segmentation/infer_video.py -c ckpt/0.segformer/test_video.yaml -o data.dataset.args.video_path=$i ; done

#for i in `cat /mnt/btrfs-data/venvs/ml-tutorials/repos/segmentation_inference/shrec-2022/shrec-2022-video-evaluation-file-list.txt` ; do echo $i ; ls -lh $i ; python configs/segmentation/infer_video.py -c ckpt/1.deeplabv3plus/test_video.yaml -o data.dataset.args.video_path=$i ; done

#for i in `cat /mnt/btrfs-data/venvs/ml-tutorials/repos/segmentation_inference/shrec-2022/shrec-2022-video-evaluation-file-list.txt` ; do echo $i ; ls -lh $i ; python configs/cps/infer_video.py -c ckpt/2.maskedsoftcps-dlunet/test_video.yaml -o data.dataset.args.video_path=$i ; done

exit 0
